﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace is_as_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //now working with the is operator

            Water_Bottle bottle_1 = new Water_Bottle();

            Water_Bottle_2 bottle_2 = new Water_Bottle_2();

            
            //using the is operator. It will return true if the bottle_2 can be treated as Water_Bottle type
            if( bottle_2 is Water_Bottle)
            {
                Console.WriteLine("Yes, bottle_2 can be converted to Water_Bottle type");

            }
            else
            {
                Console.WriteLine("No, bottle_2 cannot be converted to Water_Bottle type");
            }

            //using the as operator
            //by using the as operator, I was able to assign bottle_2 (which is Water_Bottle_2 type) to a bottle_1 
            bottle_1 = bottle_2 as Water_Bottle;            

            

        }
    }

    class Water_Bottle  //I still dont understand my obsession with using water bottle for all my class designs
    {
        public string water_quantity;

        public Water_Bottle()
        {
            water_quantity = "10";
        }

        //writing the explicit converter
        public static implicit operator string(Water_Bottle bottle)
        {
            return bottle.water_quantity;
        }
    }

    class Water_Bottle_2 : Water_Bottle
    {

    }
}
